# 👍 Apueste sobrio y no vaya en tilt
  
Aunque apostar estando sobrio parece de sentido común, te sorprendería saber cuánta gente apuesta en deportes bajo la influencia. ¿Es una coincidencia que los casinos de Las Vegas ofrezcan bebidas alcohólicas gratis mientras juega? No lo creo. No quiero sonar como tu madre aquí, pero apostar mientras estás bajo la influencia afectará tu juicio y definitivamente debes evitarlo si quieres convertirte en un apostador deportivo ganador.

La segunda parte de estos consejos es "No se incline". Ir en tilt o "tilt" es un término comúnmente conocido con los jugadores de póquer y básicamente significa dejar que sus emociones tomen el control, lo que a menudo conduce a malas decisiones. Si está enojado o molesto por algo, como un par de bad beats con sus apuestas deportivas, no debería realizar más apuestas. Tómese un descanso, salga a caminar, aclare su mente antes de comenzar a hacer más selecciones de apuestas deportivas.

Más sobre el tema [aquí.](https://www.apuestasenlinea.co/estrategias/handicap-asiatico/)